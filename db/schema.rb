# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151209132849) do

  create_table "companies", force: :cascade do |t|
    t.string   "name",            limit: 100,   null: false
    t.string   "fname",           limit: 150
    t.string   "address",         limit: 100
    t.string   "uraddress",       limit: 100
    t.string   "phone",           limit: 50
    t.string   "contract",        limit: 25
    t.string   "docsaddress",     limit: 100
    t.string   "invoiceaddress",  limit: 100
    t.string   "gmocode",         limit: 10
    t.string   "reutercode",      limit: 5
    t.string   "email",           limit: 50
    t.string   "leid",            limit: 5
    t.text     "comments",        limit: 65535
    t.integer  "company_type_id", limit: 4,     null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "companies", ["company_type_id"], name: "index_companies_on_company_type_id", using: :btree

  create_table "company_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "name",       limit: 50,                    null: false
    t.string   "jobtitle",   limit: 50
    t.string   "phone",      limit: 50
    t.string   "mobphone",   limit: 50
    t.string   "email",      limit: 50
    t.text     "comments",   limit: 65535
    t.integer  "company_id", limit: 4,                     null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "inactive",                 default: false, null: false
  end

  add_index "people", ["company_id"], name: "index_people_on_company_id", using: :btree
  add_index "people", ["name"], name: "index_people_on_name", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_foreign_key "companies", "company_types"
  add_foreign_key "people", "companies"
end
