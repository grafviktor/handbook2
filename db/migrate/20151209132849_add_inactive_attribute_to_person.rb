class AddInactiveAttributeToPerson < ActiveRecord::Migration
  def change
    add_column :people, :inactive, :boolean, null: false, default: false
  end
end
