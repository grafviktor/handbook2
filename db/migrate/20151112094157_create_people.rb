class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name, limit: 50, null: false
      t.string :jobtitle, limit: 50
      t.string :phone, limit: 50
      t.string :mobphone, limit: 50
      t.string :email, limit: 50
      t.text :comments
      t.references :company, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
    add_index :people, :name
  end
end
