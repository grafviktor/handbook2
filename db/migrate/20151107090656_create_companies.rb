class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name, limit: 100, null: false
      t.string :fname, limit: 150
      t.string :address, limit: 100
      t.string :uraddress, limit: 100
      t.string :phone, limit: 50
      t.string :contract, limit: 25
      t.string :docsaddress, limit: 100
      t.string :invoiceaddress, limit: 100
      t.string :gmocode, limit: 10
      t.string :reutercode, limit: 5
      t.string :email, limit: 50
      t.string :leid, limit: 5
      t.text :comments
      t.references :company_type, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
    # add_index :companies, :company_type_id
  end
end
