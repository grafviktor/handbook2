require 'test_helper'

class CompanyTypeTest < ActiveSupport::TestCase

  def setup

  end

  test "type must be present" do
    ct = CompanyType.new
    assert_not ct.valid?
  end

  test "length must be greater than 2" do
    ct = CompanyType.new(name: "t")
    assert_not ct.valid?
  end

  test "duplicates are not allowed" do
    ct = CompanyType.new(name: "test")
    ct.save
    ct = CompanyType.new(name: "test")
    assert_not ct.valid?
  end
end
