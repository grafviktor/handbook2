module CompaniesHelper
  def str_is_email?(str)
    if str.nil?
      return false
    end
    return str.match(/\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i)
  end

  def color_active_company(company, id)
    # binding.pry
    if(company.id == id.to_i)
      return 'class=current-company'
    end
    return ''
  end
end
