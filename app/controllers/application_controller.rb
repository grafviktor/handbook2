class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :user_is_authenticated?
  before_action :login_required!

  private
    def login_required!
      if !user_is_authenticated?
        flash[:danger] = 'Необходимо выполнить Вход в систему!'
        redirect_to root_path
      end
    end

  def user_is_authenticated?
    !!session[:user_id]
  end
end
