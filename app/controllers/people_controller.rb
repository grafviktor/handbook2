class PeopleController < ApplicationController

  before_action :init

  def edit
  end

  def destroy
    @person.destroy
    flash[:success] = 'Объект был удален.'
    redirect_to edit_company_path(params[:company_id], company_type: @company.company_type_id)
  end

  def create
    if(@company.people.build(person_params).save)
      flash[:success] = 'Объект успешно сохранен.'
      redirect_to edit_company_path(params[:company_id], company_type: @company.company_type_id)
    else
      render 'edit'
    end
  end

  def new
    render 'edit'
  end

  def show
    render 'edit'
  end

  def update
    if (@person.update_attributes(person_params))
      flash[:success] = 'Объект успешно сохранен.'
      redirect_to edit_company_path(params[:company_id], company_type: @company.company_type_id)
    else
      render 'edit'
    end
  end

  private
  def person_params
    params.require(:person).permit(:name, :jobtitle, :phone,
                                   :mobphone, :email, :comments, :inactive)

  end

  def init
    @company = Company.find(params[:company_id])
    existing_person = params[:id]
    if (existing_person)
      @person = Person.find(existing_person)
    else
      @person = Person.new
    end
  end
end
