class CompaniesController < ApplicationController

  before_action :init

  def index
  end

  def destroy
    @company.destroy
    flash[:success] = 'Объект был удален.'
    redirect_to companies_path(company_type: @company_type)
  end

  def create
    # FIXME: Duplicates init method
    company_type = CompanyType.find(@company_type)
    @company = company_type.companies.build(company_params)
    if (@company.save)
      flash[:success] = 'Объект успешно сохранен.'
      redirect_to company_path(@company, company_type: @company_type)
    else
      render 'edit'
    end
  end

  def update
    existing_company = params[:id]
    @company = Company.find(existing_company)
    if (@company.update_attributes(company_params))
      flash[:success] = 'Объект успешно сохранен.'
      redirect_to company_path(@company, company_type: @company_type)
    else
      render 'edit'
    end
  end

  def new
    render 'edit'
  end

  def show
    # redirect_to action: 'edit'
    render 'edit'
  end

  def edit
  end

  private
  def init
    existing_company = params[:id]
    if(existing_company)
      @company = Company.find(existing_company)
    else
      @company = Company.new
    end
    @people = @company.people.order(:inactive, :name)
    # FIXME: пользователь может передать любую company_type из GET запроса
    @company_type = params[:company_type] || 1
    @companies = Company.includes(:people, :company_type).where('company_type_id = ?', @company_type).order(:name)
    @company_types = CompanyType.all
  end

  def company_params
    params.require(:company).permit(:name, :fname, :address,
                                    :uraddress, :phone, :contract,
                                    :docsaddress, :invoiceaddress,
                                    :gmocode, :reutercode, :email,
                                    :leid, :comments)
  end
end
