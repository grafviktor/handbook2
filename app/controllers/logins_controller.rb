class LoginsController < ApplicationController
  skip_before_action :login_required!

  def new
    @users = User.order(:name)
  end

  def create
    user = User.find(params[:name])
    password = params[:password]
    if(user.authenticate(password))
      session[:user_id] = user.id
      flash[:success] = 'Добро пожаловать!'
      redirect_to companies_path
    else
      flash.now[:danger] = 'Неверный пароль!'
      @users = User.order(:name)
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:success] = 'Вы вышли из системы.'
    redirect_to root_path
  end

end
