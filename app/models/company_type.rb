class CompanyType < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true, length: { minimum: 2, maximum: 20 }
  validates :company_type_id, null: false
  
  has_many :companies, dependent: :destroy
end
