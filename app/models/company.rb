class Company < ActiveRecord::Base
  validates :name, presence: true, length: { minimum: 2, maximum: 100 }

  belongs_to :company_type
  # has_many :people, -> { order 'name' }, dependent: :destroy
  has_many :people, dependent: :destroy
end
