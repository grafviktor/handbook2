class User < ActiveRecord::Base
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  # validates :password_digest, presence: true
  has_secure_password
end
