class Person < ActiveRecord::Base
  validates :name, presence: true, length: { minimum: 2, maximum: 50 }
  before_save :trim_email
  belongs_to :company

  private
    def trim_email
      self.email = email.strip
    end
end
